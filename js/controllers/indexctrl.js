app.controller('IndexCtrl', ['$scope','DictsService','RequestsService',function($scope,DictsService,RequestsService) {
	$scope.getTranslation = function(){
        var nepali = document.getElementById('nepali').value; //since modal doesn't take the last letter.
		DictsService.getTranslation(nepali).then(function(data){
			if(data.status=='1') {
                $scope.response=data;
            }
			else {
                $scope.response={
                    msg:nepali,
                    status:0
                };
            }
		});
	}; //end translate
    
    DictsService.totalTranslation().then(function(data){
        if(data.status=='1') {
            $scope.total=data;
        }
    }); // end total translation
    
    $scope.addRequest = function(nepali){
        RequestsService.postRequest(nepali).then(function(data){
            if(!alert('Thank you for requesting!')){window.location.reload();}
            //http://jsfiddle.net/qDhT9/ probably directory is better
        }); // addRequests
    }; //end addrequest
}]); //end MainCtrl