app.controller('RequestCtrl', ['$scope','$facebook', 'RequestsService','DictsService','UsersService', function($scope, $facebook, RequestsService, DictsService,UsersService) {
    $scope.translate_info={};
    RequestsService.getRequest().then(function(data){
        if(data.status=='1') {
            $scope.request=data;
        }
        else {
            $scope.request={
                msg:"oops, something went wrong!",
                status:0
            };
        }
    }); // end getRequest

    RequestsService.totalRequest().then(function(data){
        if(data.status=='1') {
            $scope.total=data;
        }
    }); // end total translation
    
    // select Nepali
    $scope.select = function(nepali){
        $scope.select.nepali=nepali;
        $scope.translate_info.nepali=nepali;
    }
    //Translate
    $scope.translate = function(){
      if($scope.status){
        $scope.translate_info.id=$scope.user.id;
        var kham = document.getElementById('kham').value;
        $scope.translate_info.kham=kham;
        console.log($scope.translate_info);
        DictsService.translate($scope.translate_info).then(function(data){
            if(data.status==1){
                if(!alert('Thank you for translating!')){window.location.reload();}
            }
        });//end translate
      }else{
        alert('oops, login with facebook to translate!');
      }
    }

  //*********facebook stuff ******
  $scope.$on('fb.auth.authResponseChange', function() {
      $scope.status = $facebook.isConnected();
      if($scope.status) {
        $facebook.api('/me').then(function(user) {
          $scope.user = user;
          UsersService.login($scope.user).then(function(data){
            console.log(data);
          });
        });
      }
    });

  $scope.loginToggle = function() {
      if($scope.status) {
        $facebook.logout();
        $scope.msg_fb='Please, login in with facebook and';
      } else {
        $facebook.login();
        $scope.msg_fb='';
      }
    };
    ///////end facebook stuff   ******* 

}]); //end RequestCtrl