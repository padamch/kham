app.factory('DictsService', function($http) {
	return {
		getTranslation:function(nepali){
            // SEARCH HITS
            $http({
                method:'GET',
                url:'http://ipinfo.io/json'
            }).then(function(result){
                $http({
                    method:'POST',
                    url: rootURL+'dicts/search/post',
                    data:{'nepali':nepali,'country':result.data.country,'city':result.data.city}
                }).then(function(result){
                    console.log(result.data);
                });
            });
            /////////end SEARCH HITS
            
			return $http({
                method:'GET',
                url: rootURL+'dicts/getTranslation/'+nepali
		    }).then(function(result){
		    	return result.data;
		    });
		},//end translate
        totalTranslation:function(){
            return $http({
                method:'GET',
                url:rootURL+'dicts/total'
            }).then(function(result){
               return result.data;
            });
        },//end totalTranslation
        translate:function(translate_info){
            return $http({
              method:'POST',url: rootURL+'dicts/translate', data: translate_info,
              headers: {'Content-Type': 'application/x-www-form-urlencoded'} 
            }).then(function(result){
                return result.data;
            });
        }//end add timesheet
	}//end return
 });