app.factory('UsersService', function($http) {
    return {
        login:function(login_info){
            return $http({
                method:'POST',
                url: rootURL+'users/login', data: login_info
            }).then(function(result){
                return result.data;
            });
        }
    }//end return
 });