app.factory('RequestsService', function($http) {
	return {
        getRequest:function(){
            return $http({
                method:'GET',
                url: rootURL+'requests'
            }).then(function(result){
                resultAddRequests = result.data;
                return result.data;
            });
        },//end requests
		postRequest:function(nepali){
			return $http({
                method:'POST',
                url: rootURL+'requests/post',
                data:{'nepali':nepali}
		    }).then(function(result){
                resultAddRequests = result.data;
		    	return result.data;
		    });
		},//end add requests
        totalRequest:function(){
            return $http({
                method:'GET',
                url:rootURL+'requests/total'
            }).then(function(result){
               return result.data;
            });
        }
	}//end return
 });