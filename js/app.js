"use strict";
// create the module 
var app = angular.module('ngKham', ['ngRoute','ngFacebook']);

// configure
app.config(['$routeProvider','$facebookProvider', function($routeProvider,$facebookProvider) {
	$routeProvider
		// route for the home page
		.when('/', {
			templateUrl : 'pages/home.html',
			controller  : 'IndexCtrl'
		})
		// route for the about page
		.when('/about', {
			templateUrl : 'pages/about.html',
			controller  : 'AboutCtrl'
		})
		//route for the requested page
		.when('/request', {
			templateUrl : 'pages/request.html',
			controller  : 'RequestCtrl'
		})
		// otherwise rout to home
		.otherwise({ redirectTo: '/' });
		//https://github.com/GoDisco/ngFacebook
	$facebookProvider.setCustomInit({
		cookie : true,
		xfbml : true
	});
	$facebookProvider.setVersion("v2.1");
	$facebookProvider.setAppId('1525355921066248').setPermissions(['public_profile','email']);
}]);
// run
app.run(['$rootScope', '$window', function($rootScope, $window) {
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    $rootScope.$on('fb.load', function() {
      $window.dispatchEvent(new Event('fb.load'));
    });
  }]);