<?php
require 'database.php';
class Users extends Database{	
	public function __construct(){}
	
	/*
	* It returns the users detail
	*/
	public function getUsers(){
		$db =$this->getConnection();
		$sql = "select * FROM kham_users ORDER BY name";
		try {
		    $stmt = $db->query($sql);  
		    $users = $stmt->fetchAll(PDO::FETCH_OBJ);
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($users) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
	  	} catch(PDOException $e) {
	    	return '{"msg":"'.$e->getMessage().'","status":0}';
	  	}
	}

	/*
	* It returns a user detail by id
	*/
	public function getUser($id){
		$sql = "SELECT * FROM kham_users WHERE id=:id";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("id", $id);
		    $stmt->execute();
		    $user = $stmt->fetchObject();  
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($user) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
		  } catch(PDOException $e) {
		    return '{"msg":"'.$e->getMessage().'","status":0}';
		  }
	}

	/*
	* It adds a user detail
	*/
	public function login($users){
		if ($this->checkSnId($users->id)=='0'){
			$msg = $this->addUser($users);
			return $msg;
		} else{ 
			return '{"msg":"User already exists","status":1}';
		}
	}

	/*
	* It adds a user detail
	*/
	public function addUser($users){	
		$id='NULL';
		$created_date=date('Y-m-d');
		$role='basic';
		$oauth_provider='facebook';

		$sql = "INSERT INTO kham_users VALUES
		(:id,:sn_id,:first_name,:last_name,:name,:email,:profile_link,:oauth_provider,:role,:created_date)";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql); 
		    $stmt->bindParam("id", $id); 
		    $stmt->bindParam("sn_id", $users->id);
		    $stmt->bindParam("first_name", $users->first_name);
		    $stmt->bindParam("last_name", $users->last_name);   
		    $stmt->bindParam("name", $users->name);
		    $stmt->bindParam("email", $users->email);
		    $stmt->bindParam("profile_link", $users->link);
		    $stmt->bindParam("oauth_provider", $oauth_provider); 
		    $stmt->bindParam("role", $role);
		    $stmt->bindParam("created_date", $created_date);
		    $stmt->execute();
		    $db = null;
		    return '{"msg": "new user added","status":1}';
		   	//return json_encode($users);
		} catch(PDOException $e) {
		    return '{"msg":"'.$e->getMessage().'","status":0}';
		}
	}
	/*
	* It checks username
	*/
	public function checkSnId($sn_id){
		$sql = "SELECT sn_id FROM kham_users WHERE sn_id=:sn_id";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("sn_id", $sn_id);
		    $stmt->execute();
		    $rowCount = $stmt->rowCount();
		    $db = null;
		    return $rowCount; // if exists 1 else 0
		  } catch(PDOException $e) {
		    return $e->getMessage();
		  }

	}

}

?>