<?php
require 'database.php';
class Dicts extends Database{
	public function __construct(){}
    
	/*
	* It translates nepali to kham
	*/
	public function getTranslation($nepali){
        $sql = "SELECT d.nepali, d.kham, u.name, u.profile_link FROM kham_users u, kham_dicts d WHERE u.id=d.user_id AND d.nepali=:nepali";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("nepali", $nepali);
		    $stmt->execute();
		    $obj = $stmt->fetchObject();  
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($obj) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
		  } catch(PDOException $e) {
		    return '{"msg":"'.$e->getMessage().'","status":0}';
		  }
	}

	// get userid
	public function getUserId($sn_id){
		$sql ="SELECT id FROM kham_users WHERE sn_id=:sn_id";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("sn_id", $sn_id);
		    $stmt->execute();
		    $data = $stmt->fetchColumn();  
		    $db = null;
		    return $data;
		} catch(PDOException $e) {
		    return '{"msg":"'.$e->getMessage().'","status":0}';
		}
	}
    /*
	* It adds a user detail
	*/
	public function translate($data){
		$user_id = $this->getUserId($data->id);
		if ($this->checkNepali($data->nepali)=='0'){
			$id='NULL';
			$translated_date=date('Y-m-d');
			$sql = "INSERT INTO kham_dicts VALUES
			(:id,:nepali,:kham,:user_id,:translated_date)";
			try {
			    $db = $this->getConnection();
			    $stmt = $db->prepare($sql); 
			    $stmt->bindParam("id", $id); 
			    $stmt->bindParam("nepali", $data->nepali);
			    $stmt->bindParam("kham", $data->kham);
			    $stmt->bindParam("user_id", $user_id);   
			    $stmt->bindParam("translated_date", $translated_date);
			    $stmt->execute();
			    $db = null;
			    $this->deleteNepali($data->nepali); 
			    return '{"msg": "The word is tranlsated.","status":1}';
			   	//return json_encode($users);
			} catch(PDOException $e) {
			    return '{"msg":"'.$e->getMessage().'","status":0}';
			}
		} else{ return '{"msg":"Sorry, the word has already been translated!","status":0}';}
		
	}
    /*
    Total translation
    */
    public function totalTranslation(){
        $sql="SELECT COUNT(id) as 'total' FROM kham_dicts";
        try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->execute();
		    $obj = $stmt->fetchObject();  
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($obj) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
		  } catch(PDOException $e) {
		    return '{"msg":"'.$e->getMessage().'","status":0}';
		  }
    }

    /*
	* It checks if nepali word is already translated.
	*/
	public function deleteNepali($nepali){
		$sql = "DELETE FROM kham_requests  WHERE nepali=:nepali";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("nepali", $nepali);
		    $stmt->execute();
		    $db = null;
		  } catch(PDOException $e) {
		    return $e->getMessage();
		  }

	}

    /*
	* It checks if nepali word is already translated.
	*/
	public function checkNepali($nepali){
		$sql = "SELECT kham FROM kham_dicts WHERE nepali=:nepali";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("nepali", $nepali);
		    $stmt->execute();
		    $rowCount = $stmt->rowCount();
		    $db = null;
		    return $rowCount; // if exists 1 else 0
		  } catch(PDOException $e) {
		    return $e->getMessage();
		  }

	}
	/*
	* It returns the searchhits
	*/
	public function getSearchHits(){
		$db =$this->getConnection();
		$sql = "select * FROM kham_search_hits ORDER BY searched_word";
		try {
		    $stmt = $db->query($sql);  
		    $obj = $stmt->fetchAll(PDO::FETCH_OBJ);
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($obj) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
	  	} catch(PDOException $e) {
	    	return '{"msg":"'.$e->getMessage().'","status":0}';
	  	}
	}

	/*
	* Search hits
	*/

	public function postSearchHits($data){
		$id='NULL';
		$searched_date=date('Y-m-d');
		$searched_time = date('H:i:s');
		$sql = "INSERT INTO kham_search_hits VALUES
		(:id,:searched_word,:country,:city,:searched_date,:searched_time)";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql); 
		    $stmt->bindParam("id", $id); 
		    $stmt->bindParam("searched_word", $data->nepali);
		    $stmt->bindParam("country", $data->country);
		    $stmt->bindParam("city", $data->city);   
		    $stmt->bindParam("searched_date", $searched_date);
		    $stmt->bindParam("searched_time", $searched_time);
		    $stmt->execute();
		    $db = null;
		    return '{"msg": "search hits added","status":1}';
		} catch(PDOException $e) {
		    return '{"msg": "'.$e->getMessage().'","status":0}';
		  }
	}
	// it deletes search hits
	public function deleteSearchHits($data){			
		$sql = "DELETE FROM kham_search_hits WHERE id=:id";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id", $data->id); 
			$stmt->execute();
			$db = null;
			return '{"msg":"deleted!","status":1}';
		} catch(PDOException $e) {
			return '{"msg":"'.$e->getMessage().'","status":0}';
		}	
		
	}

	// it deletes search hits all
	public function deleteSearchHitsAll(){			
		$sql = "DELETE FROM kham_search_hits";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);
			$stmt->execute();
			$db = null;
			return '{"msg":"deleted all!","status":1}';
		} catch(PDOException $e) {
			return '{"msg":"'.$e->getMessage().'","status":0}';
	}	
		
	} // end class
}
?>