<?php
require 'database.php';
class Requests extends Database{
	public function __construct(){}
    
	/*
    * Add requests
    */
    public function postRequest($data){
		
		if($this->checkNepali($data->nepali)==0){
			$id='NULL';
			$created_date=date('Y-m-d');
			$sql = "INSERT INTO kham_requests VALUES
			(:id,:nepali,:created_date)";
			try {
				$db = $this->getConnection();
				$stmt = $db->prepare($sql); 
				$stmt->bindParam("id", $id); 
				$stmt->bindParam("nepali", $data->nepali);
				$stmt->bindParam("created_date", $created_date);
				$stmt->execute();
				$db = null;
				return '{"msg":"Thank you for requesting!","status":1}';
			} catch(PDOException $e) {
				return '{"msg":"'.$e->getMessage().'","status":0}';
			}
		}
		else{ return '{"msg":"Thank you for requesting!","status":1}';}	
	}

    /*
	* It translates nepali to kham
	*/
	public function getRequests(){
        $sql = "SELECT * FROM kham_requests";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->execute();
		    $obj = $stmt->fetchAll(PDO::FETCH_ASSOC); 
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($obj) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
		  } catch(PDOException $e) {
		    return '{"msg":"'.$e->getMessage().'","status":0}';
		  }
	}
    // check if nepali word already requested
	public function checkNepali($nepali){
		$sql = "SELECT nepali FROM kham_requests WHERE nepali=:nepali";
		try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->bindParam("nepali", $nepali);
		    $stmt->execute();
		    $rowCount = $stmt->rowCount();
		    $db = null;
		    return $rowCount; // if exists 1 else 0
		  } catch(PDOException $e) {
		    return $e->getMessage();
		  }

	}

    /*
    Total requests
    */
    public function totalRequest(){
        $sql="SELECT COUNT(id) as 'total' FROM kham_requests";
        try {
		    $db = $this->getConnection();
		    $stmt = $db->prepare($sql);  
		    $stmt->execute();
		    $obj = $stmt->fetchObject();  
		    $db = null;
		    $rowCount = $stmt->rowCount();
		    if($rowCount>'0')
		    	return '{"msg": ' . json_encode($obj) . ',"status":1}';
		    else
		    	return '{"msg": 0,"status":0}';
		  } catch(PDOException $e) {
		    return '{"msg":"'.$e->getMessage().'","status":0}';
		  }
    }

    // it deletes search hits
	public function deleteRequest($data){			
		$sql = "DELETE FROM kham_requests WHERE id=:id";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id", $data->id); 
			$stmt->execute();
			$db = null;
			return '{"msg":"deleted!","status":1}';
		} catch(PDOException $e) {
			return '{"msg":"'.$e->getMessage().'","status":0}';
		}	
		
	}

	// it deletes search hits all
	public function deleteRequestsAll(){			
		$sql = "DELETE FROM kham_requests";
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);
			$stmt->execute();
			$db = null;
			return '{"msg":"deleted all!","status":1}';
		} catch(PDOException $e) {
			return '{"msg":"'.$e->getMessage().'","status":0}';
		}
	}	
}// end class