<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim(array(
  'debug' => true
));

// USERS
$app->get('/users', 'getUsers');
$app->get('/users/:id', 'getUser');
$app->get('/users/check_username/:username', 'checkSnId');
$app->post('/users/login','login');

//DICTS
$app->get('/dicts/getTranslation/:nepali','getTranslation');
$app->get('/dicts/total','totalTranslation');
$app->post('/dicts/translate','translate');

$app->get('/dicts/search','getSearchHits');
$app->post('/dicts/search/post','postSearchHits');
$app->delete('/dicts/search/delete','deleteSearchHits'); // delete by id
$app->delete('/dicts/search/delete/all','deleteSearchHitsAll');

//REQUESTS
$app->get('/requests','getRequests');
$app->get('/requests/total','totalRequest');
$app->post('/requests/post','postRequest');
$app->delete('/requests/delete','deleteRequest'); // delete by id
$app->delete('/requests/delete/all','deleteRequestsAll');


$app->run();


function getUsers() {
  require './modal/users.php';
  $users = new Users;
  header('Content-Type: application/json');
  echo $users->getUsers();
  exit;
}

function getUser($id) {
  require './modal/users.php';
  $user = new Users;
  header('Content-Type: application/json');
  echo $user->getUser($id);
  exit;
}

function login(){
  $request = \Slim\Slim::getInstance()->request();
  $data = json_decode($request->getBody());
  require './modal/users.php';
  $obj = new Users;
  header('Content-Type: application/json');
  echo $obj->login($data);
  exit;
}

function checkSnId($sn_id) {
  require './modal/users.php';
  $user = new Users;
  header('Content-Type: application/json');
  echo $user->checkSnId($sn_id);
  exit;
}

function getTranslation($nepali) {
  require './modal/dicts.php';
  $dict = new Dicts;
  header('Content-Type: application/json');
  echo $dict->getTranslation($nepali);
  exit;
}

function totalTranslation() {
  require './modal/dicts.php';
  $dict = new Dicts;
  header('Content-Type: application/json');
  echo $dict->totalTranslation();
  exit;
}

function translate(){
  $request = \Slim\Slim::getInstance()->request();
  $data = json_decode($request->getBody());
  require './modal/dicts.php';
  $obj = new Dicts;
  header('Content-Type: application/json');
  echo $obj->translate($data);
  exit;
}

function getSearchHits() {
  require './modal/dicts.php';
  $obj = new Dicts;
  header('Content-Type: application/json');
  echo $obj->getSearchHits();
  exit;
}
function postSearchHits(){
  $request = \Slim\Slim::getInstance()->request();
  $data = json_decode($request->getBody());
  require './modal/dicts.php';
  $obj = new Dicts;
  header('Content-Type: application/json');
  echo $obj->postSearchHits($data);
  exit;
}

function deleteSearchHits(){
  $request = \Slim\Slim::getInstance()->request();
  $data = json_decode($request->getBody());
  require './modal/dicts.php';
  $obj = new Dicts;
  header('Content-Type: application/json');
  echo $obj->deleteSearchHits($data);
  exit;
}

function deleteSearchHitsAll(){
  require './modal/dicts.php';
  $obj = new Dicts;
  header('Content-Type: application/json');
  echo $obj->deleteSearchHitsAll();
  exit;
}

//REQUESTS
function getRequests() {
  require './modal/requests.php';
  $obj = new Requests;
  header('Content-Type: application/json');
  echo $obj->getRequests();
  exit;
}

function totalRequest() {
  require './modal/requests.php';
  $request = new Requests;
  header('Content-Type: application/json');
  echo $request->totalRequest();
  exit;
}
function postRequest(){
  $request = \Slim\Slim::getInstance()->request();
  $data = json_decode($request->getBody());
  require './modal/requests.php';
  $obj = new Requests;
  header('Content-Type: application/json');
  echo $obj->postRequest($data);
  exit;
}
function deleteRequest(){
  $request = \Slim\Slim::getInstance()->request();
  $data = json_decode($request->getBody());
  require './modal/requests.php';
  $obj = new Requests;
  header('Content-Type: application/json');
  echo $obj->deleteRequest($data);
  exit;
}

function deleteRequestsAll(){
  $request = \Slim\Slim::getInstance()->request();
  $data = json_decode($request->getBody());
  require './modal/requests.php';
  $obj = new Requests;
  header('Content-Type: application/json');
  echo $obj->deleteRequestsAll();
  exit;
}

?>
