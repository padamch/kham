"use strict";
// create the module 
var app = angular.module('ngAdmin', ['ngRoute']);
app.config(function($routeProvider){
	$routeProvider
		.when('/', {
			templateUrl : 'pages/search.html',
			controller  : 'SearchCtrl'
		})
		.when('/request', {
			templateUrl : 'pages/request.html',
			controller  : 'RequestCtrl'
		})
		.otherwise({ redirectTo: '/' });
});

app.controller('SearchCtrl',['$scope','$http', function($scope,$http) {
	// DO SOMETHING
	$http.get(rootURL+'dicts/search')
    	.success(function(data){
      		console.log(data.msg);
      		$scope.searchHits = data.msg;
    });
    $scope.deleteAll = function(){
    	$http({method:'DELETE', url:rootURL+'dicts/search/delete/all'})
    	.success(function(data){
    		console.log(data);
    	});
    };
}]);


app.controller('RequestCtrl',['$scope','$http', function($scope,$http) {
		// DO SOMETHING
	$http.get(rootURL+'requests')
    	.success(function(data){
      		console.log(data.msg);
      		$scope.requests = data.msg;
    });
    $scope.deleteAll = function(){
    	$http({method:'DELETE', url:rootURL+'requests/delete/all'})
    	.success(function(data){
    		console.log(data);
    	});
    };
}]);

app.directive('removeOnClick', function($http) {
    return {
        link: function(scope, elt, attrs) {
            scope.remove = function(id,x) {
				switch (x) {
					case 0:
					$http({method:'DELETE',url:rootURL+'dicts/search/delete',data:{'id':id}}).success(function(data){
	            		elt.remove();
	            		console.log(data.msg);
            		});
					break;
					case 1:
					$http({method:'DELETE',url:rootURL+'requests/delete',data:{'id':id}}).success(function(data){
	            		elt.remove();
	            		console.log(data.msg);
            		});
					break;
					case 0:
					$http({method:'DELETE',url:rootURL+'dicts/search/delete',data:{'id':id}}).success(function(data){
	            		elt.remove();
	            		console.log(data.msg);
            		});
					break;
				} 	
            }; // end romove
        }
    }
});
